#include "ApplicativeObjects.h"

Abstraction::Abstraction(std::vector<std::shared_ptr<Variable>> vars, std::shared_ptr<CombObject> structure) :
	structure(structure), vars(vars)
{}

std::shared_ptr<CombObject> Abstraction::GetStructure()
{
	return structure;
}

std::vector<std::shared_ptr<Variable>> Abstraction::GetVars()
{
	return vars;
}

std::shared_ptr<CombObject> Abstraction::Copy(VarMap& varMap)
{
	std::vector<std::shared_ptr<Variable>> varsCopy;
	VarMap vMap;

	for (auto it = vars.begin(); it != vars.end(); ++it)
	{
		auto varMap = VarMap();
		auto varCopy = std::static_pointer_cast<Variable>((*it)->Copy(varMap));
		varsCopy.push_back(varCopy);
		vMap.insert(std::make_pair(*it, varCopy));
	}
	std::shared_ptr<CombObject> structureCopy = structure->Copy(vMap);

	std::shared_ptr<CombObject> abstractionCopy = std::make_shared<Abstraction>(varsCopy, structureCopy);
	return abstractionCopy;
}

std::string Abstraction::Print()
{
	using namespace std;

	string result = "(\\";

	for (auto varsIt = vars.begin(); varsIt != vars.end(); ++varsIt)
	{
		result += (*varsIt)->GetName();
		result += ' ';
	}
	result += '.';
	result += structure->Print();
	result += ')';

	return result;
}


Combinator::Combinator(std::string name, std::vector<std::shared_ptr<Variable>> vars, std::shared_ptr<CombObject> structure) :
	name(name), vars(vars), structure(structure)
{}

std::shared_ptr<CombObject> Combinator::GetStructure()
{
	return structure;
}

std::vector<std::shared_ptr<Variable>> Combinator::GetVars()
{
	return vars;
}

std::string Combinator::GetName()
{
	return name;
}

std::shared_ptr<CombObject> Combinator::Copy(VarMap & varMap)
{
	std::vector<std::shared_ptr<Variable>> varsCopy;
	VarMap vMap;

	for (auto it = vars.begin(); it != vars.end(); ++it)
	{
		auto varMap = VarMap();
		auto varCopy = std::static_pointer_cast<Variable>((*it)->Copy(varMap));
		varsCopy.push_back(varCopy);
		vMap.insert(std::make_pair(*it, varCopy));
	}
	std::shared_ptr<CombObject> structureCopy = structure->Copy(vMap);

	std::shared_ptr<CombObject> combCopy = std::make_shared<Combinator>(name, varsCopy, structureCopy);
	return combCopy;
}

std::string Combinator::Print()
{
	return name;
}

Variable::Variable(std::string name, bool isValueSet) : 
	name(name), isValueSet(isValueSet), isValueRead(false)
{}

Variable::Variable(std::string name, std::shared_ptr<CombObject> val, bool isValueRead) : 
	name(name), value(val), isValueRead(isValueRead)
{}

std::string Variable::GetName()
{
	return name;
}

void Variable::SetValue(std::shared_ptr<CombObject> obj)
{
	value = obj;
	isValueSet = true;
}

std::shared_ptr<CombObject> Variable::GetValue(bool changeState)
{
	if (changeState) isValueRead = true;
	return value;
}

bool Variable::IsValueRead()
{
	return isValueRead;
}

bool Variable::IsValueSet()
{
	return isValueSet;
}

std::shared_ptr<CombObject> Variable::Copy(VarMap & varMap)
{
	std::shared_ptr<CombObject> varCopy;
	auto ptr = shared_from_this();
	auto it = varMap.find(ptr);

	if (it != varMap.end())
		varCopy = it->second;
	else
	{
		if (isValueSet)
		{
			std::shared_ptr<CombObject> valCopy = value->Copy(varMap);
			varCopy = std::make_shared<Variable>(name, valCopy, isValueRead);
		}
		else
		{
			varCopy = std::make_shared<Variable>(name);
		}
		auto p = std::make_pair(shared_from_this(), std::static_pointer_cast<Variable>(varCopy));
		varMap.insert(p);
	}
	return varCopy;
}

std::string Variable::Print()
{
	if (isValueSet)
		return value->Print();
	else
		return name;
}

Application::Application(std::shared_ptr<CombObject> func, std::vector<std::shared_ptr<CombObject>> args) :
	func(func), args(args)
{}

std::string Application::Print()
{
	using namespace std;
	string argsString = "";

	for (auto argsIt = args.begin(); argsIt != args.end(); ++argsIt)
	{
		argsString += ' ';
		argsString += (*argsIt)->Print();
	}

	string result = "(";

	result += func->Print();
	result += argsString;
	result += ')';

	return result;
}

std::shared_ptr<CombObject> Application::Copy(VarMap& varMap)
{
	std::shared_ptr<CombObject> funcCopy = func->Copy(varMap);
	std::vector<std::shared_ptr<CombObject>> argsCopy;

	for (auto it = args.begin(); it != args.end(); ++it)
		argsCopy.push_back((*it)->Copy(varMap));

	return std::make_shared<Application>(funcCopy, argsCopy);
}

std::shared_ptr<CombObject> Application::GetFunc()
{
	return func;
}

std::vector<std::shared_ptr<CombObject>> Application::GetArgs()
{
	return args;
}

std::shared_ptr<CombObject> Reduction::Reduce(std::shared_ptr<CombObject> obj, bool& out_isReduced, 
	std::vector<std::shared_ptr<CombObject>> args)
{
	std::shared_ptr<CombObject> result;

	if(auto app = std::dynamic_pointer_cast<Application>(obj))
	{
		auto appArgs = app->GetArgs();
		appArgs.insert(appArgs.end(), args.begin(), args.end());
		return Reduce(app->GetFunc(), out_isReduced, appArgs);
	}
	else if(auto comb = std::dynamic_pointer_cast<Combinator>(obj))
	{
		auto combVars = comb->GetVars();
		auto combVarIt = combVars.begin();
		auto combVarEnd = combVars.end();
		auto argIt = args.begin();
		auto argEnd = args.end();
		int setVarNum = 0;

		for (; combVarIt != combVarEnd; ++combVarIt)
			if ((*combVarIt)->IsValueSet())
				setVarNum++;
			else
				break;

		if (combVars.size() - setVarNum > args.size())
		{
			out_isReduced = false;

			for (; argIt != argEnd; ++argIt)
			{
				auto reducedArg = Reduce(*argIt, out_isReduced);
				if (out_isReduced)
				{
					*argIt = reducedArg;
					break;
				}
			}

			return std::make_shared<Application>(comb, args);
		}

		for (; combVarIt != combVarEnd; ++combVarIt)
		{
			(*combVarIt)->SetValue(*argIt);
			++argIt;
		}

		auto remArgs = std::vector<std::shared_ptr<CombObject>>(argIt, argEnd);

		out_isReduced = true;

		if (remArgs.size() > 0)
			return std::make_shared<Application>(comb->GetStructure(), remArgs);
		else
			return comb->GetStructure();
	}
	else if(auto abstraction = std::dynamic_pointer_cast<Abstraction>(obj))
	{
		if (args.size() > 0)
		{
			out_isReduced = true;

			auto argsIt = args.begin();
			auto abstrArgs = abstraction->GetVars();
			auto abstrArgsIt = abstrArgs.begin();

			for (; argsIt != args.end() && abstrArgsIt != abstrArgs.end(); ++argsIt, ++abstrArgsIt)
				(*abstrArgsIt)->SetValue(*argsIt);

			if(args.size() < abstrArgs.size())
			{
				auto newAbstrArgs = std::vector<std::shared_ptr<Variable>>(abstrArgsIt, abstrArgs.end());
				return std::make_shared<Abstraction>(newAbstrArgs, abstraction->GetStructure());
			}
			else if (args.size() > abstrArgs.size())
			{
				auto appArgs = std::vector<std::shared_ptr<CombObject>>(argsIt, args.end());
				return std::make_shared<Application>(abstraction->GetStructure(), appArgs);
			}
			else
				return abstraction->GetStructure();
		}
		else
		{
			auto newStructure = Reduce(abstraction->GetStructure(), out_isReduced);
			
			if (out_isReduced)
				return std::make_shared<Abstraction>(abstraction->GetVars(), newStructure);
			
			return abstraction;
		}
	}
	else if(auto var = std::dynamic_pointer_cast<Variable>(obj))
	{
		if (var->IsValueSet())
			return Reduce(var->GetValue(), out_isReduced, args);

		out_isReduced = false;
		if (args.size() > 0)
		{
			for (auto argIt = args.begin(); argIt != args.end(); ++argIt)
			{
				auto reducedArg = Reduce(*argIt, out_isReduced);
				if (out_isReduced)
				{
					*argIt = reducedArg;
					break;
				}
			}

			return std::make_shared<Application>(var, args);
		}
		else
			return var;
	}
}

std::shared_ptr<CombObject> Reduction::Reduce(std::shared_ptr<CombObject> obj,
	int argNum, bool& out_isReduced, std::vector<std::shared_ptr<CombObject>> args)
{
	std::shared_ptr<CombObject> result;

	auto app = std::dynamic_pointer_cast<Application>(obj);
	auto var = std::dynamic_pointer_cast<Variable>(obj);
	if(app)
	{
		if(argNum == 0)
			return Reduce(app->GetFunc(), out_isReduced, app->GetArgs());
		else
		{
			auto appArgs = app->GetArgs();
			if(argNum <= appArgs.size())
			{
				appArgs[argNum - 1] = Reduce(appArgs[argNum - 1], out_isReduced);
				return std::make_shared<Application>(app->GetFunc(), appArgs);
			}
		}

	} else if (var)
	{
		if (var->IsValueSet())
			return Reduce(var->GetValue(), argNum, out_isReduced, args);
	}
	return obj;
}
