#ifndef APPLICATIVEOBJECTS_H
#define APPLICATIVEOBJECTS_H

#include <map>
#include <vector>
#include <memory>

class Variable;
typedef std::map<std::shared_ptr<Variable>, std::shared_ptr<Variable>> VarMap;

class CombObject
{
public:
	virtual std::shared_ptr<CombObject> Copy(VarMap & varMap) = 0;
	virtual std::string Print() = 0;
};

class Abstraction : public CombObject
{
public:
	Abstraction(std::vector<std::shared_ptr<Variable>> vars, std::shared_ptr<CombObject> structure);
	std::shared_ptr<CombObject> GetStructure();
	std::vector<std::shared_ptr<Variable>> GetVars();
	std::shared_ptr<CombObject> Copy(VarMap & varMap);
	std::string Print();

private:
	std::vector<std::shared_ptr<Variable>> vars;
	std::shared_ptr<CombObject> structure;
};

class Combinator : public CombObject
{
public:
	virtual ~Combinator() {};
	Combinator(std::string name, std::vector<std::shared_ptr<Variable>> vars, std::shared_ptr<CombObject> structure);
	std::shared_ptr<CombObject> GetStructure();
	std::vector<std::shared_ptr<Variable>> GetVars();
	std::string GetName();
	std::shared_ptr<CombObject> Copy(VarMap & varMap);
	std::string Print();

private:
	std::string name;
	std::vector<std::shared_ptr<Variable>> vars;
	std::shared_ptr<CombObject> structure;
};

class Variable : public CombObject, public std::enable_shared_from_this<Variable>
{
public:
	Variable(std::string name, bool isValueSet = false);
	Variable(std::string name, std::shared_ptr<CombObject> val, bool isValueRead);
	std::string GetName();
	void SetValue(std::shared_ptr<CombObject> obj);
	bool IsValueSet();
	bool IsValueRead();
	std::shared_ptr<CombObject> GetValue(bool changeState = true);
	std::shared_ptr<CombObject> Copy(VarMap & varMap);
	std::string Print();
private:
	std::string name;
	std::shared_ptr<CombObject> value;
	bool isValueSet;
	bool isValueRead;
};

class Application : public CombObject
{
public:
	Application(std::shared_ptr<CombObject> func, 
		std::vector<std::shared_ptr<CombObject>> args);
	std::shared_ptr<CombObject> Copy(VarMap & varMap);
	std::string Print();
	std::shared_ptr<CombObject> GetFunc();
	std::vector<std::shared_ptr<CombObject>> GetArgs();
private:
	std::shared_ptr<CombObject> func;
	std::vector<std::shared_ptr<CombObject>> args;
};

class Reduction
{
public:
	std::shared_ptr<CombObject> Reduce(std::shared_ptr<CombObject> obj, bool & out_isReduced,
		std::vector<std::shared_ptr<CombObject>> args = {});

	std::shared_ptr<CombObject> Reduce(std::shared_ptr<CombObject> obj, int argNum, bool & out_isReduced,
		std::vector<std::shared_ptr<CombObject>> args = {});
};
#endif // APPLICATIVEOBJECTS_H