#include "Parser.h"
#include <ctype.h>
#include <typeinfo>
#include <string>
#include <sstream>

Token Lexer::GetToken(std::string::const_iterator &it, std::string::const_iterator end)
{
	Token res;

	while (it != end && *it == ' ')
		++it;

	if (it == end || *it == '\n')
	{
		res.tokId = tok_eof;
		return res;
	}
	if(isalpha(*it))
	{
		res.tokId = tok_identifier;
		res.identifierStr = *it;
		++it;

		while(it != end && isalpha(*it))
		{
			res.identifierStr += *it;
			++it;
		}

		return res;
	}	

	res.tokId = *it++;
	return res;
}

AbstractionAST::AbstractionAST(std::vector<std::string> argNames, std::shared_ptr<ExprAST> body) :
	argNames(argNames), body(body)
{}

std::shared_ptr<CombObject> AbstractionAST::CombObjectGen(std::vector<std::shared_ptr<CombObject>>& existentObjs)
{
	std::vector<std::shared_ptr<Variable>> vars;

	for (auto it = argNames.begin(); it != argNames.end(); ++it)
		vars.push_back(std::make_shared<Variable>(*it));

	auto newExistentObj = std::vector<std::shared_ptr<CombObject>>(existentObjs.begin(), existentObjs.end());
	newExistentObj.insert(newExistentObj.end(), vars.begin(), vars.end());

	auto structure = body->CombObjectGen(newExistentObj);

	auto abstraction = std::make_shared<Abstraction>(vars, structure);

	return abstraction;
}


CallAST::CallAST(std::shared_ptr<ExprAST> func, std::vector<std::shared_ptr<ExprAST>> args) :
	func(func), args(args)
{}

std::shared_ptr<CombObject> CallAST::CombObjectGen(
	std::vector<std::shared_ptr<CombObject>>& existentObjs)
{
	auto appFunc = func->CombObjectGen(existentObjs);
	std::vector<std::shared_ptr<CombObject>> appArgs;

	for (auto it = args.begin(); it != args.end(); ++it)
		appArgs.push_back((*it)->CombObjectGen(existentObjs));

	return std::make_shared<Application>(appFunc, appArgs);
}


IdentifierAST::IdentifierAST(std::string id) : id(id)
{}

std::shared_ptr<CombObject> IdentifierAST::CombObjectGen(
	std::vector<std::shared_ptr<CombObject>>& existentObjs)
{
	for (auto it = existentObjs.begin(); it != existentObjs.end(); ++it)
	{
		auto comb = std::dynamic_pointer_cast<Combinator>(*it);
		
		if (comb)
		{
			if (id == comb->GetName())
			{
				auto varMap = VarMap();
				return (*it)->Copy(varMap);
			}
		}
		else
			if (id == std::static_pointer_cast<Variable>(*it)->GetName())
				return *it;
	}

	return std::make_shared<Variable>(id);
}

DeclAST::DeclAST(
	std::string funcName,
	std::vector<std::string> argNames,
	std::shared_ptr<ExprAST> body) :
	funcName(funcName), argNames(argNames), body(body)
{}

std::shared_ptr<CombObject> DeclAST::CombObjectGen(
	std::vector<std::shared_ptr<CombObject>>& existentObjs)
{
	std::vector<std::shared_ptr<Variable>> vars;

	for (auto it = argNames.begin(); it != argNames.end(); ++it)
		vars.push_back(std::make_shared<Variable>(*it));

	auto newExistentObj = std::vector<std::shared_ptr<CombObject>>(existentObjs.begin(), existentObjs.end());
	newExistentObj.insert(newExistentObj.end(), vars.begin(), vars.end());

	auto structure = body->CombObjectGen(newExistentObj);

	auto comb = std::make_shared<Combinator>(funcName, vars, structure);
	existentObjs.push_back(comb);

	return comb;
}

ErrorAST::ErrorAST(std::string error) : error(error)
{}

std::shared_ptr<CombObject> ErrorAST::CombObjectGen(
	std::vector<std::shared_ptr<CombObject>>& existentObjs)
{
	return std::make_shared<ErrorCombObj>("Ast error.");
}


std::shared_ptr<ExprAST> Parser::ParseExp(const std::string& text)
{
	auto readTokens = testToTokens(text);
	std::vector<Token>::const_reverse_iterator rIt = readTokens.rbegin();
	std::vector<Token>::const_reverse_iterator rEnd = readTokens.rend();
	return tokensToAST(rIt, rEnd);
}

std::vector<Token> Parser::testToTokens(const std::string& text)
{
	auto it = text.begin();
	auto end = text.end();
	std::vector<Token> readTokens;
	Token tok;

	while ((tok = Lexer::GetToken(it, end)).tokId != tok_eof)
		readTokens.push_back(tok);

	return readTokens;
}

std::shared_ptr<ExprAST> Parser::tokensToAST(
	std::vector<Token>::const_reverse_iterator &rIt,
	std::vector<Token>::const_reverse_iterator rEnd,
	unsigned int flags)
{
	std::vector<std::shared_ptr<ExprAST>> exprs;

	for (; rIt != rEnd; ++rIt)
	{
		std::shared_ptr<ExprAST> newExpr;

		switch (rIt->tokId)
		{
		case tok_identifier:
			exprs.push_back(std::make_shared<IdentifierAST>(rIt->identifierStr));
			break;
		
		case ')':
			newExpr = tokensToAST(++rIt, rEnd, ExpectOpenBracket);

			if (typeid(*newExpr) != typeid(CallAST))
			{
				std::stringstream errorstream;
				errorstream << "Expected " << typeid(CallAST).name() << ", but " << typeid(newExpr).name() << " was got.";
				return std::make_shared<ErrorAST>(errorstream.str());
			}

			exprs.push_back(newExpr);

			if (rIt->tokId != '(')
				return std::make_shared<ErrorAST>("Expected '('.");

			break;
		
		case '(':
		{
			if (flags & ExpectOpenBracket != ExpectOpenBracket)
				return std::make_shared<ErrorAST>("Expected ')' after '('.");

			if (exprs.size() == 0)
				return std::make_shared<ErrorAST>("Expected identifier.");

			auto exprIt = exprs.rbegin();
			newExpr = *(exprIt++);
			auto args = std::vector<std::shared_ptr<ExprAST>>(exprIt, exprs.rend());

			if (args.size() == 0)
				return newExpr;
			else
				return std::make_shared<CallAST>(newExpr, args);

			break;
		}

		case '=':
		{
			if (exprs.size() == 0)
				return std::make_shared<ErrorAST>("Expected combinator definition");

			auto declIdentifiers = std::vector<Token>(rEnd.base(), (++rIt).base());

			if (declIdentifiers.size() == 0)
				return std::make_shared<ErrorAST>("Expected combinator name.");

			auto declIt = declIdentifiers.begin();

			if (declIt->tokId != tok_identifier)
				return std::make_shared<ErrorAST>("Expected combinator name.");

			auto name = (declIt++)->identifierStr;
			std::vector<std::string> args;

			for (; declIt != declIdentifiers.end(); ++declIt)
			{
				if (declIt->tokId != tok_identifier)
					return std::make_shared<ErrorAST>("Expected argument name.");
				args.push_back(declIt->identifierStr);
			}

			//��������� � �����
			auto exprIt = exprs.rbegin();
			newExpr = *(exprIt++);
			auto cargs = std::vector<std::shared_ptr<ExprAST>>(exprIt, exprs.rend());

			if (cargs.size() != 0)
				newExpr = std::make_shared<CallAST>(newExpr, cargs);

			return std::make_shared<DeclAST>(name, args, newExpr);
			break;
		}

		case '.':
		{
			std::vector<std::string> abstractionArgs;

			if (exprs.size() == 0)
				return std::make_shared<ErrorAST>("Expected abstraction body.");

			while (++rIt != rEnd && rIt->tokId != '\\')
			{
				if (rIt->tokId != tok_identifier)
					return std::make_shared<ErrorAST>("Expected argument name.");

				abstractionArgs.push_back(rIt->identifierStr);
			}

			if (rIt->tokId != '\\')
				return std::make_shared<ErrorAST>("Expected '\\'.");

			if (abstractionArgs.size() == 0)
				return std::make_shared<ErrorAST>("Expected at least one argument name.");

			std::reverse(abstractionArgs.begin(), abstractionArgs.end());

			auto exprIt = exprs.rbegin();
			auto abstrBodyFunc = *(exprIt++);
			auto args = std::vector<std::shared_ptr<ExprAST>>(exprIt, exprs.rend());
			std::shared_ptr<ExprAST> abstrAst;

			if (args.size() == 0)
				abstrAst = std::make_shared<AbstractionAST>(abstractionArgs, abstrBodyFunc);
			else
			{
				auto abstrBody = std::make_shared<CallAST>(abstrBodyFunc, args);
				abstrAst = std::make_shared<AbstractionAST>(abstractionArgs, abstrBody);
			}

			exprs = { abstrAst };
			break;
		}

		default:
			return std::make_shared<ErrorAST>("Undefined token");
		}
	}

	auto exprIt = exprs.rbegin();
	auto newExpr = *(exprIt++);
	auto args = std::vector<std::shared_ptr<ExprAST>>(exprIt, exprs.rend());

	if (args.size() == 0)
		return newExpr;
	
	return std::make_shared<CallAST>(newExpr, args);
}


ErrorCombObj::ErrorCombObj(std::string error) : error(error)
{}

std::shared_ptr<CombObject> ErrorCombObj::Copy(VarMap& varMap)
{
	return std::make_shared<ErrorCombObj>(error);
}

std::string ErrorCombObj::Print()
{
	return error;
}


ModelGenerator::ModelGenerator(std::vector<std::shared_ptr<CombObject>> existentObjs) :
	existentObjs(existentObjs)
{}

std::shared_ptr<CombObject> ModelGenerator::AstToCombObj(std::shared_ptr<ExprAST> exprAst)
{
	return exprAst->CombObjectGen(existentObjs);
}
