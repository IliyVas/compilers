﻿#ifndef PARSER_H
#define PARSER_H

#include "ApplicativeObjects.h"
#include <string>
#include <vector>
#include <memory>

enum TokId
{
	tok_eof = -1,
	tok_identifier = -2,
};

struct Token
{
	int tokId;
	std::string identifierStr;
};

class Lexer
{
public:
	static Token GetToken(std::string::const_iterator &it, std::string::const_iterator end);
};

struct ExprAST
{
	virtual ~ExprAST() {}

	virtual std::shared_ptr<CombObject> 
	CombObjectGen(std::vector<std::shared_ptr<CombObject>> & existentObjs) = 0;
};

struct CallAST : ExprAST
{
	CallAST(std::shared_ptr<ExprAST> func, std::vector<std::shared_ptr<ExprAST>> args = {});
	virtual std::shared_ptr<CombObject>
	CombObjectGen(std::vector<std::shared_ptr<CombObject>> & existentObjs);

	std::shared_ptr<ExprAST> func;
	std::vector<std::shared_ptr<ExprAST>> args;
};

struct AbstractionAST : ExprAST
{
	AbstractionAST(std::vector<std::string> argNames, std::shared_ptr<ExprAST> body);
	virtual std::shared_ptr<CombObject>
	CombObjectGen(std::vector<std::shared_ptr<CombObject>> & existentObjs);

	std::vector<std::string> argNames;
	std::shared_ptr<ExprAST> body;
};

struct IdentifierAST : ExprAST
{
	IdentifierAST(std::string id);
	virtual std::shared_ptr<CombObject>
	CombObjectGen(std::vector<std::shared_ptr<CombObject>> & existentObjs);

	std::string id;
};

struct DeclAST : public ExprAST 
{
	DeclAST(std::string funcName, std::vector<std::string> argNames, std::shared_ptr<ExprAST> body);
	virtual std::shared_ptr<CombObject>
	CombObjectGen(std::vector<std::shared_ptr<CombObject>> & existentObjs);

	std::string funcName;
	std::vector<std::string> argNames;
	std::shared_ptr<ExprAST> body;
};

struct ErrorAST : ExprAST
{
	ErrorAST(std::string error);
	virtual std::shared_ptr<CombObject>
	CombObjectGen(std::vector<std::shared_ptr<CombObject>> & existentObjs);

	std::string error;
};

enum ParseFlags
{
	None = 0,
	ExpectOpenBracket = 1,
};

class Parser
{
public:
	static std::shared_ptr<ExprAST> ParseExp(const std::string & text);

private:
	static std::vector<Token> testToTokens(const std::string & text);
	static std::shared_ptr<ExprAST> tokensToAST(
		std::vector<Token>::const_reverse_iterator &rIt,
		std::vector<Token>::const_reverse_iterator rEnd,
		unsigned int flags = 0);
};

struct ErrorCombObj : CombObject
{
	ErrorCombObj(std::string error);
	std::shared_ptr<CombObject> Copy(VarMap & varMap);
	std::string Print();

	std::string error;
};

class ModelGenerator
{
public:
	ModelGenerator(std::vector<std::shared_ptr<CombObject>> existentObjs = {});

	std::shared_ptr<CombObject> AstToCombObj(std::shared_ptr<ExprAST> exprAst);

private:
	std::vector<std::shared_ptr<CombObject>> existentObjs;
};

#endif // PARSER_H