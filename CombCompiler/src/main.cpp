#include <iostream>
#include "ApplicativeObjects.h"
#include <string>
#include <Parser.h>

int main(int argc, char **argv)
{
	/*Reduction r;

	auto x1 = std::make_shared<Variable>("x");
	auto y1 = std::make_shared<Variable>("y");
	std::vector<std::shared_ptr<Variable>> KArgs;
	KArgs.push_back(x1);
	KArgs.push_back(y1);
	auto K = std::make_shared<Combinator>("K", KArgs, x1);

	auto x2 = std::make_shared<Variable>("x");
	auto y2 = std::make_shared<Variable>("y");
	auto z2 = std::make_shared<Variable>("z");
	std::vector<std::shared_ptr<Variable>> SArgs = {x2, y2, z2};
	std::vector<std::shared_ptr<CombObject>> args2 = {z2};
	auto appYZ = std::make_shared<Application>(y2, args2);
	std::vector<std::shared_ptr<CombObject>> args3 = { z2, appYZ };
	
	auto SApp = std::make_shared<Application>(x2, args3);
	auto S = std::make_shared<Combinator>("S", SArgs, SApp);

	std::vector<std::shared_ptr<CombObject>> KK = { K, K->Copy() };
	auto Istr = std::make_shared<Application>(S->Copy(), KK);
	auto I = std::make_shared<Combinator>("I", std::vector<std::shared_ptr<Variable>>(), Istr);

	auto a = std::make_shared<Variable>("a");

	std::vector<std::shared_ptr<CombObject>> args4 = {I, I->Copy() };
	auto obj1 = std::make_shared<Application>(S->Copy(), args4);
	std::vector<std::shared_ptr<CombObject>> arg = { obj1, obj1->Copy(), };

	auto obj2 = std::make_shared<Application>(S->Copy(), arg);
	std::vector<std::shared_ptr<CombObject>> arg2 = { obj1 };

	auto obj = std::make_shared<Application>(obj1->Copy(), arg2);
	std::cout << obj->Print().c_str() << std::endl;

	int i = 0;
	while (i != -1)
	{
		std::cin >> i;
		if (i >= 0)
		{
			bool isRed = false;
			obj = std::static_pointer_cast<Application>(r.Reduce(obj, i, isRed));
			std::cout << obj->Print().c_str() << std::endl;
		}
	}*/

	std::string input = "";
	Reduction r;
	ModelGenerator mgenerator = ModelGenerator();

	while (1)
	{
		std::getline(std::cin, input);

		if (input == "") continue;
		if (input == "stop") break;

		auto ast = Parser::ParseExp(input);
		auto obj = mgenerator.AstToCombObj(ast);

		if (std::dynamic_pointer_cast<CallAST>(ast))
		{
			int i = 0;
			while (i != -1)
			{
				std::cin >> i;
				if (i >= 0)
				{
					bool isRed = false;
					obj = std::static_pointer_cast<Application>(r.Reduce(obj, i, isRed));
					std::cout << obj->Print().c_str() << std::endl;
				}
			}
		}
	}

	return 0;
}
