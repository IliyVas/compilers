#include "MainWindowHandler.h"
#include "Parser.h"
#include <iostream>

bool MainWindowHandler::calcFinished() const
{
    return m_calcFinished;
}

void MainWindowHandler::setCalcFinished(bool val)
{
    if(val != m_calcFinished)
    {
        m_calcFinished = val;
        emit calcFinishedChanged();
    }
}

QString MainWindowHandler::run(QString code)
{
    Program pr;
    
    try
    {
        if(Parser::Parse(code.toStdString(), pr))
        {
            Executor executor = Executor(pr);
            CombObject res = executor.Execute();

            auto visitor = CombObjGetStringVisitor(false);
            return  QString("Result: ") 
                    + QString::fromStdString(boost::apply_visitor(visitor, res));
        }
        else
            throw std::exception();
    }
    catch(...)
    {
        return QString("Can't get the result");
    }
}

QString MainWindowHandler::combToStr(const CombObject &obj, bool showVar)
{
    auto visitor = CombObjGetStringVisitor(showVar);
    return QString::fromStdString(boost::apply_visitor(visitor, obj));
}

QVariantList MainWindowHandler::load(QString code)
{
    Program pr;
    QVariantList result;
    
    try
    {
        if(Parser::Parse(code.toStdString(), pr))
        {
            stepExecutor = std::shared_ptr<ParallelExecutor>(new ParallelExecutor(pr));
            threadIds = stepExecutor->GetIdsVector();
            
            result << combToStr(stepExecutor->GetProgramObj(), false);
            
            for(std::vector<int>::iterator it = threadIds.begin();
                it != threadIds.end();
                ++it)
            {
                result << combToStr(stepExecutor->GetThreadObj(*it), false);
            }
            
            setCalcFinished(false);
        }
        else
            throw std::exception();
    }
    catch(...)
    {
        result << QString("");
    }
    
    return result;
}

QString MainWindowHandler::makeStep(int tabIndex)
{
    QString result;
    
    if(tabIndex == 0)
    {
        stepExecutor->ReduceOneStep(0);
        
        if(stepExecutor->IsReduceFinished(0))
        {
            result =    QString("Result: ") 
                        + combToStr(stepExecutor->GetProgramObj(), false)
                        + QString("\nFinished.");
            
            setCalcFinished(true);
        }
        else
            if(stepExecutor->HasBeenReduced(0))
                return combToStr(stepExecutor->GetProgramObj(), false);
            else
                return QString("Wait");
    }
    else
    {
        int index = tabIndex - 1;
        int thr_id = threadIds[index];
        if(stepExecutor->IsReduceFinished(thr_id))
        {
            stepExecutor->ReduceOneStep(thr_id);
            threadIds.erase(threadIds.begin() + index);
            result = QString("");
        }
        else
        {
            stepExecutor->ReduceOneStep(thr_id);            
            result = combToStr(stepExecutor->GetThreadObj(thr_id), false);
            if(stepExecutor->IsReduceFinished(thr_id))
                result = "Result: " + result;
        }
    }
    
    return result;
}

QString MainWindowHandler::update(int tabIndex)
{
    if(tabIndex == 0)
    {
        if(stepExecutor->HasBeenReduced(0))
            return combToStr(stepExecutor->GetProgramObj(), false);
        else
            return QString("");
    }
    else
    {
        int index = tabIndex - 1;
        int thr_id = threadIds[index];
        if(stepExecutor->HasBeenReduced(thr_id))
            return combToStr(stepExecutor->GetThreadObj(thr_id), false);
        else
            return QString("");
    }
}