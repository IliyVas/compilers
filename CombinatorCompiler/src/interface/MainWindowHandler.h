#ifndef MAINWINDOWHANDLER_H
#define MAINWINDOWHANDLER_H

#include <QQuickTextDocument>
#include <QtGui/QTextCharFormat>
#include <QtCore/QTextCodec>
#include <qqmlfile.h>
#include <QVariant>
#include "ParallelExecutor.h"

class MainWindowHandler : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool calcFinished READ calcFinished WRITE setCalcFinished NOTIFY calcFinishedChanged)

public:
    bool calcFinished() const;

public slots:
    Q_INVOKABLE QString run(QString code);
    Q_INVOKABLE QVariantList load(QString code);
    Q_INVOKABLE QString makeStep(int tabIndex);
    Q_INVOKABLE QString update(int tabIndex);
    void setCalcFinished(bool val);

signals:
    void calcFinishedChanged();

private:
    bool m_calcFinished;
    std::shared_ptr<ParallelExecutor> stepExecutor;
    std::vector<int> threadIds;
    
    QString combToStr(const CombObject &obj, bool showVar);
};

#endif // MAINWINDOWHANDLER_H