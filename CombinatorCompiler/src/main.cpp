#include <iostream>
#include <QGuiApplication>
#include <QtQml>
#include <QQmlApplicationEngine>
#include "MainWindowHandler.h"
#include "ApplicativeObjects.h"

int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);
    qmlRegisterType<MainWindowHandler>("org.qtproject.mwhandler", 1, 0, "MainWindowHandler");
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/resources/qml/MainWindow.qml")));
    return app.exec();
}