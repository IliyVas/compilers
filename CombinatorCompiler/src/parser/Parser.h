#ifndef PARSER_H
#define PARSER_H

#include <string>
#include <vector>
#include <stdio.h>
#include <QtCore/qabstractitemmodel.h>
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/foreach.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/filesystem.hpp>

namespace fusion    = boost::fusion;
namespace phoenix   = boost::phoenix;
namespace qi        = boost::spirit::qi;
namespace ascii     = boost::spirit::ascii;

enum DefaultComb { SUM, SUB, DIV, MULT, ASSIGN };

struct Statement;
typedef boost::variant<double, Statement> ArgumentObject;

typedef std::string SuperCombName;
typedef boost::variant<DefaultComb, SuperCombName> Function;

struct Statement
{
    Function function;
    std::vector<ArgumentObject> args;
};

struct NamedCombinator
{
    std::string name;
    std::vector<std::string> args;
    Statement reduction;
};

typedef std::vector<Statement> Program;

BOOST_FUSION_ADAPT_STRUCT(
    Statement,
    (Function, function)
    (std::vector<ArgumentObject>, args)
)

BOOST_FUSION_ADAPT_STRUCT(
    NamedCombinator,
    (std::string, name)
    (std::vector<std::string>, args)
    (Statement, reduction)
)

template <typename Iterator>
struct CombStatementGrammar : qi::grammar<Iterator, Program(), qi::blank_type>
{
    CombStatementGrammar() : CombStatementGrammar::base_type(program, "program")
    {
        using qi::eps;
        using qi::eol;
        using qi::eoi;
        using qi::lit;
        using qi::no_skip;
        using qi::lexeme;
        using qi::repeat;
        using ascii::char_;
        using ascii::string;
        using ascii::alnum;
        using ascii::alpha;
        using qi::on_error;
        using qi::fail;
        using boost::spirit::double_;
        using phoenix::construct;
        using phoenix::val;
        using namespace qi::labels;

        using phoenix::at_c;
        using phoenix::push_back;

        defaultComb.add
                ("SUM",     SUM)
                ("SUB",     SUB)
                ("DIV",     DIV)
                ("MULT",    MULT)
                ;

        program =
                -((assigment[push_back(_val, _1)] % eol) >> eol)
            >>  statement [push_back(_val, _1)]
        ;
        
        assigment = 
                func_stmt       [push_back(at_c<1>(_val), _1)]
            >>  *var_stmt       [push_back(at_c<1>(_val), _1)]
            >>  '=' >> eps      [at_c<0>(_val) = ASSIGN]
            >>  statement       [push_back(at_c<1>(_val), _1)]
        ;
        
        func_stmt = combNameOrVar[at_c<0>(_val) = _1];        
        var_stmt =  varName[at_c<0>(_val) = _1];
        
        combNameOrVar %= lexeme[+alpha];
        
        varName %= lexeme[+alpha];        
        
        statement %= stmt_with_par | no_par_stmt;
        
        stmt_with_par %= '(' >> no_par_stmt >> ')';
        
        no_par_stmt %= 
                func
            >>  *arg
        ;
        
        func %= defaultComb | combNameOrVar;
        
        arg %= double_ | stmt_with_par | single_stmt;
        
        single_stmt %= var_stmt | func_stmt;
        
    }
    qi::rule<Iterator, Program(), qi::blank_type> program;
    qi::rule<Iterator, Statement(), qi::blank_type> statement;
    qi::rule<Iterator, Statement(), qi::blank_type> stmt_with_par;        
    qi::rule<Iterator, Statement(), qi::blank_type> func_stmt;    
    qi::rule<Iterator, Statement(), qi::blank_type> var_stmt;
    qi::rule<Iterator, Statement(), qi::blank_type> assigment;
    qi::rule<Iterator, Statement(), qi::blank_type> single_stmt;
    qi::rule<Iterator, std::string(), qi::blank_type> combNameOrVar;
    qi::rule<Iterator, std::string(), qi::blank_type> varName;
    qi::rule<Iterator, Statement(), qi::blank_type> no_par_stmt;
    qi::rule<Iterator, ArgumentObject(), qi::blank_type> arg;
    qi::rule<Iterator, Function(), qi::blank_type> func;
    qi::symbols<char, DefaultComb> defaultComb;
};

class Parser
{
public:
    static bool Parse(std::string text, Program &outProgram);
};

#endif // PARSER_H
