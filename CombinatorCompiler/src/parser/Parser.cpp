#include "Parser.h"

bool Parser::Parse(std::string text, Program &outProgram)
{
    typedef CombStatementGrammar<std::string::const_iterator> CombStatementGrammar;
    CombStatementGrammar grammar;
    std::string::const_iterator iter = text.begin();
    std::string::const_iterator end = text.end();
    
    using boost::spirit::qi::blank;
    bool r = qi::phrase_parse(iter, end, grammar, blank, outProgram);

    return r;
}