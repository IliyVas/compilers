#include "ApplicativeObjects.h"
#include <string>

int Variable::counter = 1;

Variable::Variable()
{
    name = "var";
    name += std::to_string(counter);
    counter++;
}

std::string Variable::GetName()
{
    return name;
}

Application::Application(CombObject &function, std::vector<CombObject> &arguments) 
    : func(function), args(arguments)
{}

CombObjReduceVisitor::CombObjReduceVisitor()
{}

CombObjReduceVisitor::CombObjReduceVisitor(std::vector<CombObject> &args) :
        arguments(args)
{}

void CombObjReduceVisitor::SetArguments(std::vector<CombObject> &args)
{
    arguments = args;
}

CombObject CombObjReduceVisitor::operator()(double &val) const
{
    return CombObject(val);
}

CombObject CombObjReduceVisitor::operator()(int &val) const
{
    return CombObject(val);
}

CombObject CombObjReduceVisitor::operator()(std::shared_ptr<Combinator> &comb) const
{
    return comb->Reduce(arguments);
}

CombObject CombObjReduceVisitor::operator()(Application &app) const
{
    std::vector<CombObject> vect = std::vector<CombObject>();
    vect.insert(vect.end(), arguments.begin(), arguments.end());
    vect.insert(vect.end(), app.args.begin(), app.args.end());
    auto visitor = CombObjReduceVisitor(vect);
    return boost::apply_visitor(visitor, app.func);
}

CombObject CombObjReduceVisitor::operator()(std::shared_ptr<Variable> &var) const
{
    return boost::apply_visitor(*this, var->Value);
}

double CombObjGetNumVisitor::operator()(double &val) const
{
    return val;
}

double CombObjGetNumVisitor::operator()(int &val) const
{
    return static_cast<double>(val);
}

double CombObjGetNumVisitor::operator()(std::shared_ptr<Combinator> &comb) const
{
    CombObject result = comb->Reduce(std::vector<CombObject>());
    return boost::apply_visitor(CombObjGetNumVisitor(), result);
}

double CombObjGetNumVisitor::operator()(Application &app) const
{
    CombObject result = boost::get<std::shared_ptr<Combinator>>(app.func)->Reduce(app.args);
    return boost::apply_visitor(CombObjGetNumVisitor(), result);
}


double CombObjGetNumVisitor::operator()(std::shared_ptr<Variable> &var) const
{
    return boost::apply_visitor(CombObjGetNumVisitor(), var->Value);
}

CombObjGetStringVisitor::CombObjGetStringVisitor(bool showVarNames) : 
        getVarNames(showVarNames), isFirst(true)
{}

std::string CombObjGetStringVisitor::operator()(const int &val) const
{
    return std::to_string(val);
}

std::string CombObjGetStringVisitor::operator()(const double &val) const
{
    std::string result = std::to_string(val);
    result.erase ( result.find_last_not_of('0') + 1, std::string::npos );
    result.erase ( result.find_last_not_of('.') + 1, std::string::npos );
    return result;    
}

std::string CombObjGetStringVisitor::operator()(const std::shared_ptr<Combinator> &comb) const
{
    return comb->Name();
}

std::string CombObjGetStringVisitor::operator()(const Application &app)
{
    using namespace std;
    vector<string> args;
    bool tmpIsFirst = isFirst;
    isFirst = false;
    
    for(vector<string>::size_type i = 0; i < app.args.size(); ++i)
        args.push_back(boost::apply_visitor(*this, app.args[i]));
    
    string args_string = "";
    for (vector<string>::size_type i = 0; i < args.size(); ++i)
    {
        args_string += ' ';
        args_string += args[i];
    }
    
    string result = "";
    
    if(!tmpIsFirst) result += '(';
    result += boost::apply_visitor(*this, app.func);
    result += args_string;
    if(!tmpIsFirst) result += ')';
    
    return result;    
}

std::string CombObjGetStringVisitor::operator()(const std::shared_ptr<Variable> &var) const
{
    if(getVarNames)
        return var->GetName();
    
    else
    {
        auto visitor = CombObjGetStringVisitor(getVarNames);
        return boost::apply_visitor(visitor, var->Value);
    }
}

CombObject Combinator::Reduce(const std::vector<CombObject> &args)
{
    return 0;
}

std::string Combinator::Name()
{
    return "";
}

SuperCombinator::SuperCombinator(std::string comb_name) : name(comb_name), numberOfBindArgs(0)
{}

CombObject SuperCombinator::Reduce(const std::vector<CombObject> &args)
{
    CombObject result;
    
    if(arguments.size() - numberOfBindArgs >= args.size())
    {
        for(int i = 0; i < args.size(); ++i)
        {
            arguments[i]->Value = args[i];
            numberOfBindArgs++;
        }
        if(numberOfBindArgs == arguments.size())
        {
            numberOfBindArgs = 0;
            result = boost::apply_visitor(CombObjReduceVisitor(), structure);
        }
        else
            result = meta.find(name)->second;
    }
    else
    {
        int numOfBindArgs = arguments.size() - numberOfBindArgs;
        
        for(int i = 0; i < arguments.size() - numberOfBindArgs; ++i)
        {
            arguments[i]->Value = args[i];
            numberOfBindArgs++;
        }
        
        auto app_args = std::vector<CombObject>(args.begin() + numOfBindArgs, args.end());
        CombObject app = Application(structure, app_args);
        result = boost::apply_visitor(CombObjReduceVisitor(), app);
    }
    
    return result;
}

void SuperCombinator::SetStructure(CombObject &obj)
{
    structure = obj;
}

std::string SuperCombinator::Name()
{
    return name;
}

std::map<std::string, CombObject> ObjWithMeta::meta = {};