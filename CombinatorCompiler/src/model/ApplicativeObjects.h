#ifndef APPLICATIVEOBJECTS_H
#define APPLICATIVEOBJECTS_H

#include <boost/variant.hpp>
#include <map>

class Combinator;
struct Application;
class Variable;

using boost::recursive_wrapper;

typedef boost::variant<
    recursive_wrapper<std::shared_ptr<Combinator>>, 
    recursive_wrapper<Application>, 
    recursive_wrapper<std::shared_ptr<Variable>>, 
    int, 
    double
> CombObject;

class Variable
{
public:
    CombObject Value;
    Variable();
    std::string GetName();
private:
    std::string name;
    static int counter;
};

class CombObjGetNumVisitor : public boost::static_visitor<double>
{
public:
    double operator()(double &val) const;
    double operator()(int &val) const;
    double operator()(std::shared_ptr<Combinator> &comb) const;
    double operator()(Application &app) const;
    double operator()(std::shared_ptr<Variable> &var) const;
};

class CombObjGetStringVisitor : public boost::static_visitor<std::string>
{
public:
    CombObjGetStringVisitor(bool getVarNames);

    std::string operator()(const double &val) const;
    std::string operator()(const int &val) const;
    std::string operator()(const std::shared_ptr<Combinator> &comb) const;
    std::string operator()(const Application &app);
    std::string operator()(const std::shared_ptr<Variable> &var) const;

private:
    bool getVarNames;
    bool isFirst;
};

class CombObjReduceVisitor : public boost::static_visitor<CombObject>
{
public:
    CombObjReduceVisitor();
    CombObjReduceVisitor(std::vector<CombObject> &args);
    void SetArguments(std::vector<CombObject> &args);

    CombObject operator()(double &val) const;
    CombObject operator()(int &val) const;
    CombObject operator()(std::shared_ptr<Combinator> &comb) const;
    CombObject operator()(Application &app) const;
    CombObject operator()(std::shared_ptr<Variable> &var) const;

private:
    std::vector<CombObject> arguments;
};

struct ObjWithMeta
{
    static std::map<std::string, CombObject> meta;
};

struct Application : public ObjWithMeta
{
    CombObject func;
    std::vector<CombObject> args;
    
    Application(CombObject &function, std::vector<CombObject> &arguments);
};

class Combinator : public ObjWithMeta
{
public:
    virtual CombObject Reduce(const std::vector<CombObject> &args);
    virtual std::string Name();
};

class SuperCombinator : public Combinator
{
public:
    SuperCombinator(std::string comb_name);
    CombObject Reduce(const std::vector<CombObject> &args);
    void SetStructure(CombObject &obj);
    std::string Name();
    
    std::vector<std::shared_ptr<Variable>> arguments;
private:
    std::string name;
    CombObject structure;
    int numberOfBindArgs;
};
#endif // APPLICATIVEOBJECTS_H