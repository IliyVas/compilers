#include "AlgebraicCombinators.h"

CombObject SumCombinator::Reduce(const std::vector<CombObject> &args)
{
    CombObject first = args[0];
    CombObject second = args[1];
    double firstArg = boost::apply_visitor(CombObjGetNumVisitor(), first);
    double secondArg = boost::apply_visitor(CombObjGetNumVisitor(), second);
    CombObject result = firstArg + secondArg;
    return result;
}

std::string SumCombinator::Name()
{
    return "SUM";
}

CombObject SubCombinator::Reduce(const std::vector<CombObject> &args)
{
    CombObject first = args[0];
    CombObject second = args[1];
    double firstArg = boost::apply_visitor(CombObjGetNumVisitor(), first);
    double secondArg = boost::apply_visitor(CombObjGetNumVisitor(), second);
    CombObject result = firstArg - secondArg;
    return result;
}

std::string SubCombinator::Name()
{
    return "SUB";
}

CombObject MultCombinator::Reduce(const std::vector<CombObject> &args)
{
    CombObject first = args[0];
    CombObject second = args[1];
    double firstArg = boost::apply_visitor(CombObjGetNumVisitor(), first);
    double secondArg = boost::apply_visitor(CombObjGetNumVisitor(), second);
    CombObject result = firstArg * secondArg;
    return result;
}

std::string MultCombinator::Name()
{
    return "MULT";
}

CombObject DivCombinator::Reduce(const std::vector<CombObject> &args)
{
    CombObject first = args[0];
    CombObject second = args[1];
    double firstArg = boost::apply_visitor(CombObjGetNumVisitor(), first);
    double secondArg = boost::apply_visitor(CombObjGetNumVisitor(), second);
    CombObject result = firstArg / secondArg;
    return result;
}

std::string DivCombinator::Name()
{
    return "DIV";
}