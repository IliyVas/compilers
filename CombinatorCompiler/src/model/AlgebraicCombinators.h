#ifndef ALGEBRAICCOMBINATORS_H
#define ALGEBRAICCOMBINATORS_H

#include "ApplicativeObjects.h"

class SumCombinator : public Combinator
{
public:
    CombObject Reduce(const std::vector<CombObject> &args);
    std::string Name();
};

class SubCombinator : public Combinator
{
public:
    CombObject Reduce(const std::vector<CombObject> &args);
    std::string Name();
};

class MultCombinator : public Combinator
{
public:
    CombObject Reduce(const std::vector<CombObject> &args);
    std::string Name();
};

class DivCombinator : public Combinator
{
public:
    CombObject Reduce(const std::vector<CombObject> &args);
    std::string Name();
};

#endif // ALGEBRAICCOMBINATORS_H