#include "Executor.h"
#include "ApplicativeObjects.h"
#include <iostream>
#include <algorithm>
#include <functional>
#include <utility>
#include <boost/variant.hpp>

const std::map<DefaultComb, std::shared_ptr<Combinator>> Executor::combinators  = {
        { SUM,  std::shared_ptr<Combinator>(new SumCombinator())  },
        { SUB,  std::shared_ptr<Combinator>(new SubCombinator())  },
        { MULT, std::shared_ptr<Combinator>(new MultCombinator()) },
        { DIV,  std::shared_ptr<Combinator>(new DivCombinator())  }
    };

Executor::Executor(Program p) : isReduceFinished(false), isReduced(false)
{
    ObjWithMeta::meta.clear();
    
    for(std::vector<Statement>::size_type i = 0; i < p.size() - 1; ++i)
        statementToObj(p[i]);
        
    programObj = statementToObj(p.back());
}

Executor::Executor(CombObject &obj) :
    isReduceFinished(false), 
    isReduced(false),
    programObj(obj)
{
    ObjWithMeta::meta.clear();
}

CombObject Executor::Execute()
{
    programObj = boost::apply_visitor(CombObjGetNumVisitor(), programObj);
    isReduceFinished = true;
    return programObj;
}

CombObject Executor::GetProgramObj()
{
    return programObj;
}

void Executor::ReduceOneStep()
{
    auto visitor = Executor::OneStepReduceVisitor(false);
    programObj = boost::apply_visitor(visitor, programObj);
    isReduced = visitor.GetIsReduced();
    
    if(!isReduced)
        isReduceFinished = true;
}

bool Executor::HasBeenReduced()
{
    bool tmp = isReduced;
    isReduced = false;
    
    return tmp;
}

bool Executor::IsReduceFinished()
{
    return isReduceFinished;
}

CombObject Executor::statementToObj(Statement stmt, 
                     std::map<std::string, std::shared_ptr<Variable>> varWithNames)
{
    auto visitor = FunctionCombVisitor(stmt.args, varWithNames);
    return boost::apply_visitor(visitor, stmt.function);
}

Executor::ArgumentGetCombVisitor::ArgumentGetCombVisitor(
        std::map<std::string, 
        std::shared_ptr<Variable>> varWithNamesMap) : varWithNames(varWithNamesMap)
{}

CombObject Executor::ArgumentGetCombVisitor::operator()(Statement &stmt) const
{
    return Executor::statementToObj(stmt, varWithNames);
}

CombObject Executor::ArgumentGetCombVisitor::operator()(double &val) const
{
    CombObject result = val;
    return result;
}

Executor::FunctionCombVisitor::FunctionCombVisitor(
        std::vector<ArgumentObject> &args, 
        std::map<std::string, std::shared_ptr<Variable>> varWithNamesMap
        ) : arguments(args), varWithNames(varWithNamesMap)
{}

CombObject Executor::FunctionCombVisitor::operator()(DefaultComb &comb)
{
    using namespace boost;
    using boost::get;
    
    CombObject result;
    
    switch(comb)
    {
        case ASSIGN:
        {
            auto sc_name = get<std::string>(get<Statement>(arguments[0]).function);
            SuperCombinator *new_scomb = new SuperCombinator(sc_name);
            
            for(std::vector<ArgumentObject>::size_type i = 1; i < arguments.size() - 1; ++i)
            {
                auto new_var = std::shared_ptr<Variable>(new Variable());
                new_scomb->arguments.push_back(new_var);
                auto  p = std::make_pair(get<std::string>(get<Statement>(arguments[i]).function), new_var);
                varWithNames.insert(p);
            }
            
            auto last = arguments.back();
            CombObject structure = statementToObj(boost::get<Statement>(last), varWithNames);
            new_scomb->SetStructure(structure);  
            
            auto ptrToComb = std::shared_ptr<Combinator>(new_scomb);
            ObjWithMeta::meta.insert(std::make_pair(sc_name, ptrToComb));
            
            result = ptrToComb;
            break;
        }
        
        default:
            CombObject new_comb = combinators.find(comb)->second;

            std::vector<CombObject> comb_args;
            //std::transform(stmt.args.begin(), stmt.args.end(), args.begin(), [this](Statement obj) { return this->statementToObj(obj); });
            auto visitor = ArgumentGetCombVisitor(varWithNames);
            
            if(arguments.size() > 0)
            {
                for (std::vector<ArgumentObject>::size_type i = 0; i < arguments.size(); ++i)
                {
                    CombObject new_arg = boost::apply_visitor(visitor, arguments[i]);
                    comb_args.push_back(new_arg);    
                } 
                
                result = Application(new_comb, comb_args);
            }
            else
                result = new_comb;
            break;
    }
    
    return result;
}

CombObject Executor::FunctionCombVisitor::operator()(SuperCombName &comb_name)
{
    CombObject result;
    auto iter = ObjWithMeta::meta.find(comb_name);
    
    if(iter == ObjWithMeta::meta.end())
    {
        auto var_iter = varWithNames.find(comb_name);
        if(var_iter == varWithNames.end())
            throw std::exception();
        
        else
            result = var_iter->second;
    }
    else
        result = iter->second;
        
    if(arguments.size() > 0)
    {
        std::vector<CombObject> comb_args;
        auto visitor = ArgumentGetCombVisitor(varWithNames);
        
        for (std::vector<ArgumentObject>::size_type i = 0; i < arguments.size(); ++i)
        {
            CombObject new_arg = boost::apply_visitor(visitor, arguments[i]);
            comb_args.push_back(new_arg);    
        } 
        
        result = Application(result, comb_args);
    }
        
    return result;
}

Executor::OneStepReduceVisitor::OneStepReduceVisitor(bool isCombStructureAccounted) : 
        isReduced(false), isStructAccounted(isCombStructureAccounted)
{}

Executor::OneStepReduceVisitor::OneStepReduceVisitor(
        bool isCombStructureAccounted, std::vector<CombObject> &args
    ) :     isReduced(false), 
            isStructAccounted(isCombStructureAccounted),
            arguments(args)
{}

bool Executor::OneStepReduceVisitor::GetIsReduced()
{
    return isReduced;
}

CombObject Executor::OneStepReduceVisitor::operator()(double &val)
{
    return val;
}

CombObject Executor::OneStepReduceVisitor::operator()(int &val)
{
    return val;
}
CombObject Executor::OneStepReduceVisitor::operator()(std::shared_ptr<Combinator> &comb)
{
    CombObject result;
    
    if(arguments.size() > 0)
    {
        for(int i = 0; i < arguments.size() && !isReduced; ++i)
        {
            auto visitor = OneStepReduceVisitor(isStructAccounted);
            CombObject res = boost::apply_visitor(visitor, arguments[i]);
            if(visitor.GetIsReduced())
            {
                arguments[i] = res;
                isReduced = true;
                CombObject func = comb;
                result = Application(func, arguments);
            }
        }
        
        if(!isReduced)
        {   
            result = comb->Reduce(arguments);
            isReduced = true;
        }
    }
    else
        result = comb;
        
    return result;
}

CombObject Executor::OneStepReduceVisitor::operator()(Application &app)
{
    std::vector<CombObject> vect = std::vector<CombObject>();
    vect.insert(vect.end(), app.args.begin(), app.args.end());
            
    auto visitor = OneStepReduceVisitor(isStructAccounted, vect);
    CombObject result =  boost::apply_visitor(visitor, app.func);
    isReduced = visitor.GetIsReduced();
    
    return result;
}

CombObject Executor::OneStepReduceVisitor::operator()(std::shared_ptr<Variable> &var)
{
    CombObject result = boost::apply_visitor(*this, var->Value);
    if(isReduced)
        return result;
    else
        return var;
}