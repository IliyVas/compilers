#ifndef EXECUTOR_H
#define EXECUTOR_H

#include <map>
#include <thread>
#include <future>
#include "Parser.h"
#include "AlgebraicCombinators.h"
#include "ApplicativeObjects.h"

class Executor
{
public:
    Executor(Program p);
    Executor(CombObject &obj);
    
    CombObject Execute();
    CombObject GetProgramObj();
    void ReduceOneStep();
    bool HasBeenReduced();
    bool IsReduceFinished();
    
    static CombObject statementToObj(
        Statement stmt, 
        std::map<std::string, std::shared_ptr<Variable>> varWithNames = 
            std::map<std::string, std::shared_ptr<Variable>>()
    );
    
protected:
    CombObject programObj;
    
    bool isReduced;
    bool isReduceFinished;
    
    static const std::map<DefaultComb, std::shared_ptr<Combinator>> combinators;
    
    class ArgumentGetCombVisitor : public boost::static_visitor<CombObject>
    {
    public:
        ArgumentGetCombVisitor(std::map<std::string, 
                               std::shared_ptr<Variable>> varWithNamesMap);
        
        CombObject operator()(double &val) const;                             
        CombObject operator()(Statement &stmt) const;
        
    private:
        std::map<std::string, std::shared_ptr<Variable>> varWithNames;
    };

    class FunctionCombVisitor : public boost::static_visitor<CombObject>
    {
    public:
        FunctionCombVisitor(std::vector<ArgumentObject> &args, 
                        std::map<std::string, std::shared_ptr<Variable>> varWithNamesMap);
    
        CombObject operator()(DefaultComb &comb);
        CombObject operator()(SuperCombName &comb_name);
    
    private:
        std::vector<ArgumentObject> arguments;
        std::map<std::string, std::shared_ptr<Variable>> varWithNames;
    };
    
    class OneStepReduceVisitor : public boost::static_visitor<CombObject>
    {
    public:
        OneStepReduceVisitor(bool isCombStructureAccounted);
        OneStepReduceVisitor(bool isCombStructureAccounted, std::vector<CombObject> &args);
        bool GetIsReduced();
    
        CombObject operator()(double &val);
        CombObject operator()(int &val);
        CombObject operator()(std::shared_ptr<Combinator> &comb);
        CombObject operator()(Application &app);
        CombObject operator()(std::shared_ptr<Variable> &var);
    
    private:
        bool isReduced;
        bool isStructAccounted;
        std::vector<CombObject> arguments;
    };
};
#endif // EXECUTOR_H