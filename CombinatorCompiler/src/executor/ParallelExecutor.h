#ifndef PARALLELEXECUTOR_H
#define PARALLELEXECUTOR_H

#include "Executor.h"
#include <mutex>
#include <thread>

class ParallelExecutor : public Executor
{
public:
    ParallelExecutor(Program p);
    
    void ReduceOneStep(int threadId);
    bool HasBeenReduced(int threadId);
    bool IsReduceFinished(int threadId);

    std::vector<int> GetIdsVector();  
    CombObject GetThreadObj(int threadId);

private:

    class StopableThread
    {
    public:
        StopableThread(int thrId, CombObject &obj);
        ~StopableThread();
        int GetId();
        CombObject GetCombObj();
        void ReduceOneStep();
        bool HasBeenReduced();
        bool IsReduceFinished();
        
    private:
        Executor executor;
        void threadFun();
        int id;
        std::mutex m;
        std::thread thr;
    };
    
    typedef int ThrId;
    
    class CombObjThrCreateVisitor : public boost::static_visitor<>
    {
    public:    
        CombObjThrCreateVisitor(
                std::map<ThrId, std::shared_ptr<StopableThread>> *threadsMap);
        void operator()(Application &app);
        void operator()(std::shared_ptr<Variable> &var);
        
        template<typename T>
        void operator()(T &val);
    private:
        std::map<ThrId, std::shared_ptr<StopableThread>> * threads;
    };
    
    class CombObjReplaceArgVisitor : public boost::static_visitor<>
    {
    public:    
        CombObjReplaceArgVisitor(StopableThread *stThr);
        
        void operator()(Application &app);
        void operator()(std::shared_ptr<Variable> &var);
        
        template<typename T>
        void operator()(T &val);
    
    private:
        StopableThread *thr;
    };
    
    std::map<ThrId, bool> isThreadReduced; 
    std::map<ThrId, std::shared_ptr<StopableThread>> threads; 
};


#endif // PARALLELEXECUTOR_H
