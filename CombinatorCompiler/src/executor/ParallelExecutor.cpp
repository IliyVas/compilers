#include "ParallelExecutor.h"
#include <chrono>

using namespace std::literals;

ParallelExecutor::ParallelExecutor(Program p) : Executor(p)
{
    auto visitor = CombObjThrCreateVisitor(&threads);
    boost::apply_visitor(visitor, programObj);
}

void ParallelExecutor::ReduceOneStep(int threadId)
{
    if(threadId == 0)
    {
        if(threads.size() > 0)
            isThreadReduced[0] = false;
        
        else
        {
            auto visitor = OneStepReduceVisitor(false);
            programObj = boost::apply_visitor(visitor, programObj);
            
            isThreadReduced[0] = visitor.GetIsReduced();
            
            if(!visitor.GetIsReduced())
                isReduceFinished = true;                
        }
    }
    else
    {
        auto thread_ptr = threads.find(threadId)->second;
        if(thread_ptr->IsReduceFinished())
        {
            if(isThreadReduced[threadId])
                isThreadReduced[0] = true;
            StopableThread * ptr = thread_ptr.get();
            auto visitor = CombObjReplaceArgVisitor(ptr);
            boost::apply_visitor(visitor, programObj);
            threads.erase(threadId);
            isThreadReduced.erase(threadId);
        }
        else
        {
            thread_ptr->ReduceOneStep();
            if(thread_ptr->HasBeenReduced())
                isThreadReduced[threadId] = true;
        }
    }
}

bool ParallelExecutor::HasBeenReduced(int threadId)
{
    bool tmp = isThreadReduced.find(threadId)->second;
    isThreadReduced[threadId] = false;
    return tmp;
}

bool ParallelExecutor::IsReduceFinished(int threadId)
{
    if(threadId == 0)
        return isReduceFinished;
    else
        return threads.find(threadId)->second->IsReduceFinished();
}

std::vector<int> ParallelExecutor::GetIdsVector()
{
    std::vector<int> result;
    
    for(std::map<ThrId, std::shared_ptr<StopableThread>>::iterator it = threads.begin();
        it != threads.end();
        ++it) 
    {
        result.push_back(it->first);
    }
    return result;
}

CombObject ParallelExecutor::GetThreadObj(int threadId)
{
    if(threadId == 0)
        return programObj;
    else
        return threads.find(threadId)->second->GetCombObj();
}

ParallelExecutor::CombObjReplaceArgVisitor::CombObjReplaceArgVisitor(
    StopableThread *stThr
) : thr(stThr)
{}

void ParallelExecutor::CombObjReplaceArgVisitor::operator()(Application &app)
{
    app.args[thr->GetId() - 1] = thr->GetCombObj();
}

void ParallelExecutor::CombObjReplaceArgVisitor::operator()(std::shared_ptr<Variable> &var)
{
    boost::apply_visitor(*this, var->Value);
}

template<typename T>
void ParallelExecutor::CombObjReplaceArgVisitor::operator()(T &val)
{
    return;
}

ParallelExecutor::CombObjThrCreateVisitor::CombObjThrCreateVisitor(
        std::map<ThrId, std::shared_ptr<StopableThread>> *threadsMap) : threads(threadsMap)
{}

void ParallelExecutor::CombObjThrCreateVisitor::operator()(Application &app)
{
    for(std::vector<CombObject>::size_type i = 0; i < app.args.size(); ++i)
    {
        auto st_thr = std::shared_ptr<StopableThread>(
            new StopableThread(i + 1, app.args[i])
        );
        threads->insert(std::make_pair(st_thr->GetId(), st_thr));
    }
}

void ParallelExecutor::CombObjThrCreateVisitor::operator()(std::shared_ptr<Variable> &var)
{
    boost::apply_visitor(*this, var->Value);
}

template<typename T>
void ParallelExecutor::CombObjThrCreateVisitor::operator()(T &val)
{
    return;
}

ParallelExecutor::StopableThread::StopableThread(int thrId, CombObject &obj) :
        id(thrId), executor(Executor(obj))
{
    m.lock();
    thr = std::thread(&ParallelExecutor::StopableThread::threadFun, this);
}

ParallelExecutor::StopableThread::~StopableThread()
{
    executor.Execute();
    m.unlock();
    thr.join();
}

int ParallelExecutor::StopableThread::GetId()
{
    return id;
}

CombObject ParallelExecutor::StopableThread::GetCombObj()
{
    return executor.GetProgramObj();
}

bool ParallelExecutor::StopableThread::HasBeenReduced()
{
    return executor.HasBeenReduced();
}

bool ParallelExecutor::StopableThread::IsReduceFinished()
{
    return executor.IsReduceFinished();
}

void ParallelExecutor::StopableThread::threadFun()
{
    while(!executor.IsReduceFinished())
    {
        m.lock();
        executor.ReduceOneStep();
        m.unlock();
        std::this_thread::sleep_for(10ms);
    }
}

void ParallelExecutor::StopableThread::ReduceOneStep()
{
    m.unlock(); 
    std::this_thread::sleep_for(10ms);
    m.lock();
    
}